package io.escapism.ui.view;

import com.vaadin.ui.Label;
import com.vaadin.ui.VerticalLayout;

public class DashboardView extends VerticalLayout implements ProtectedView {
    public DashboardView() {
        Label title = new Label("Escapism Dashboard");
        title.addStyleName("h1");
        addComponent(title);
    }
}
