package io.escapism.ui.components;

import com.vaadin.navigator.Navigator;
import com.vaadin.ui.SingleComponentContainer;
import com.vaadin.ui.UI;
import io.escapism.ui.view.DashboardView;
import io.escapism.ui.view.LoginView;
import io.escapism.ui.view.NewGroupView;
import io.escapism.ui.view.RegisterView;

public final class EscapismNavigator extends Navigator {
    public static final String DASHBOARD = "Dashboard";
    public static final String NEW_GROUP = "NewGroup";
    public static final String LOGIN = "Login";
    public static final String REGISTER = "Register";

    public EscapismNavigator(UI ui, SingleComponentContainer container) {
        super(ui, container);
        addView(DASHBOARD, new DashboardView());
        addView(NEW_GROUP, new NewGroupView());
        addView(LOGIN, new LoginView());
        addView(REGISTER, new RegisterView());
    }
}
