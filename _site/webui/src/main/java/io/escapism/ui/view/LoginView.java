package io.escapism.ui.view;

import com.vaadin.event.ShortcutAction;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.server.UserError;
import com.vaadin.server.VaadinSession;
import com.vaadin.ui.*;
import com.vaadin.ui.themes.ValoTheme;
import io.escapism.domain.core.EMailAddress;
import io.escapism.domain.core.service.DomainRepository;
import io.escapism.domain.group.InvalidEMailAddressFormatException;
import io.escapism.domain.user.User;
import io.escapism.ui.components.EscapismNavigator;
import org.apache.commons.lang3.exception.ExceptionUtils;

import java.io.IOException;
import java.util.Objects;

public class LoginView extends VerticalLayout implements View {

    private final TextField username;
    private final PasswordField password;

    public LoginView() {
        super();

        setDefaultComponentAlignment(Alignment.MIDDLE_CENTER);

        username = new TextField("E-Mail");
        username.setRequiredIndicatorVisible(true);
        password = new PasswordField("Password");
        password.setRequiredIndicatorVisible(true);
        
        Button loginButton = new Button("login");
        loginButton.addClickListener(e1 -> loginUser());
        loginButton.setClickShortcut(ShortcutAction.KeyCode.ENTER);
        loginButton.setStyleName(ValoTheme.BUTTON_PRIMARY);

        Button registerButton = new Button("register");
        registerButton.addClickListener(e ->  UI.getCurrent().getNavigator().navigateTo(EscapismNavigator.REGISTER));
        registerButton.setStyleName(ValoTheme.BUTTON_LINK);

        FormLayout layout = new FormLayout();
        layout.addComponent(username);
        layout.addComponent(password);
        layout.setDefaultComponentAlignment(Alignment.MIDDLE_CENTER);

        Panel loginPanel = new Panel("Login");
        loginPanel.setContent(layout);
        loginPanel.setSizeUndefined();

        HorizontalLayout buttonRow = new HorizontalLayout();
        buttonRow.addComponent(loginButton);
        buttonRow.addComponent(registerButton);

        addComponent(loginPanel);
        addComponent(buttonRow);
    }

    private void loginUser() {
        EMailAddress mail = validateForm();
        if(mail == null) return;

        String userId = authenticateUser(mail);
        if(userId == null) return;

        VaadinSession.getCurrent().setAttribute("user-id", userId);

        UI.getCurrent().getNavigator().navigateTo(EscapismNavigator.DASHBOARD);
    }

    private String authenticateUser(EMailAddress mail) {
        User user;
        try {
            user = DomainRepository.user().findUserByEmail(mail);
        } catch (IOException ex) {
            Notification.show(String.format("Internal Error: %s - %s", ex.getClass().getSimpleName(), ex.getMessage()), ExceptionUtils.getStackTrace(ex), Notification.Type.ERROR_MESSAGE);
            return null;
        }


        if(user == null || !user.authenticate(password.getValue())) {
            Notification.show("Wrong username or password.", Notification.Type.ERROR_MESSAGE);
            return null;
        }

        return user.getId().toString();
    }

    private EMailAddress validateForm() {
        if(username.getValue().isEmpty()) {
            username.setComponentError(new UserError("Please enter!"));
        }

        if(password.getValue().isEmpty()) {
            password.setComponentError(new UserError("Please enter!"));
        }

        EMailAddress userId;
        try {
            userId = EMailAddress.fromString(username.getValue());
        } catch (InvalidEMailAddressFormatException e1) {
            username.setComponentError(new UserError("Invalid user id"));
            return null;
        }
        return userId;
    }

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent event) {
        String userId = (String) VaadinSession.getCurrent().getAttribute("user-id");
        if(userId != null) {
            UI.getCurrent().getNavigator().navigateTo(EscapismNavigator.DASHBOARD);
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        LoginView that = (LoginView) o;
        return Objects.equals(username, that.username) &&
                Objects.equals(password, that.password);
    }

    @Override
    public int hashCode() {

        return Objects.hash(super.hashCode(), username, password);
    }
}
