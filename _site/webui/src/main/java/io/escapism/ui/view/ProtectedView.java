package io.escapism.ui.view;

import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.server.VaadinSession;
import com.vaadin.ui.Notification;
import com.vaadin.ui.UI;
import io.escapism.ui.components.EscapismNavigator;

/**
 * The ProtectedView makes sure that the user is authenticated and has an active session. Otherwise
 * it will redirect to the Login.
 */
public interface ProtectedView extends View {
    @Override
    default void enter(ViewChangeListener.ViewChangeEvent event) {
       String userId = (String) VaadinSession.getCurrent().getAttribute("user-id");
       if(userId == null) {
           UI.getCurrent().getNavigator().navigateTo(EscapismNavigator.LOGIN);
           Notification.show("Please login.", "YOu need to be logged in to view this page.", Notification.Type.WARNING_MESSAGE);
       }
    }
}
