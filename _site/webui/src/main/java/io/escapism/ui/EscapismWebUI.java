package io.escapism.ui;

import com.vaadin.annotations.Theme;
import com.vaadin.annotations.VaadinServletConfiguration;
import com.vaadin.server.Responsive;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinServlet;
import com.vaadin.ui.Panel;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;
import io.escapism.domain.core.DomainConfiguration;
import io.escapism.infrastructure.JdbcRepository;
import io.escapism.ui.components.EscapismNavigator;
import io.escapism.ui.components.TopMenu;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import java.sql.SQLException;
import java.util.logging.Logger;

import static io.escapism.domain.core.service.DomainConfigurationService.configureDomain;

@Theme("escapism")
public class EscapismWebUI extends UI {
    private static final  Logger logger = Logger.getLogger(EscapismWebUI.class.getName());



    @Override
    protected void init(VaadinRequest vaadinRequest) {
        createUI();
    }

    private void createUI() {
        Responsive.makeResponsive(this);
        addStyleName(ValoTheme.UI_WITH_MENU);

        final VerticalLayout layout = new VerticalLayout();
        final Panel contentPanel = new Panel();
        setNavigator(new EscapismNavigator(this, contentPanel));

        layout.addComponent(new TopMenu(this));
        layout.addComponent(contentPanel);
        setContent(layout);

        getNavigator().navigateTo(EscapismNavigator.DASHBOARD);
    }


    @WebServlet(urlPatterns = "/*", name = "EscapismWebUIServlet", asyncSupported = true)
    @VaadinServletConfiguration(ui = EscapismWebUI.class, productionMode = false)
    public static class EscapismWebUIServlet extends VaadinServlet {
        @Override
        protected void servletInitialized() throws ServletException {
            JdbcRepository repository = getRepository();
            if (repository == null) return;

            DomainConfiguration config = getDomainConfiguration(repository);
            configureDomain(config);

            super.servletInitialized();
        }

        private static DomainConfiguration getDomainConfiguration(JdbcRepository repository) {
            DomainConfiguration config = new DomainConfiguration();
            config.setGroupRepository(repository);
            config.setPlayerRepository(repository);
            config.setUserRepository(repository);
            return config;
        }

        private static JdbcRepository getRepository() {
            JdbcRepository repository;
            try {
                repository = new JdbcRepository("jdbc:h2:~/escapism.h2db");
            } catch (SQLException e) {
                logger.severe(String.format("failed to access database: %s - %s", e.getClass().getSimpleName(), e.getMessage()));
                return null;
            }
            repository.addErrorHandler(new DomainRepositoryErrorHandler());
            return repository;
        }
    }
}
