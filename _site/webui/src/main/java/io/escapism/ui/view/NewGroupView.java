package io.escapism.ui.view;

import com.vaadin.icons.VaadinIcons;
import com.vaadin.server.VaadinSession;
import com.vaadin.ui.*;
import io.escapism.domain.core.EMailAddress;
import io.escapism.domain.core.service.DomainEventDispatchService;
import io.escapism.domain.core.service.DomainRepository;
import io.escapism.domain.group.*;
import io.escapism.domain.group.event.GroupCreated;
import io.escapism.domain.player.Player;
import io.escapism.ui.components.EscapismNavigator;
import org.apache.commons.lang3.exception.ExceptionUtils;

import java.io.IOException;
import java.util.Objects;
import java.util.UUID;

public class NewGroupView extends VerticalLayout implements ProtectedView {

    private final TextField groupName;
    private final Button createGroupButton;
    private final Label grpStatus;

    public NewGroupView() {
        Label heading = new Label("Create New Group");
        heading.addStyleName("h2");
        addComponent(heading);

        HorizontalLayout hlayout = new HorizontalLayout();

        groupName = new TextField("Group Name");
        groupName.addValueChangeListener(e -> checkGroupExists(e.getValue()));
        hlayout.addComponent(groupName);

        grpStatus = new Label("");
        hlayout.addComponent(grpStatus);
        addComponent(hlayout);

        createGroupButton = new Button("Create");
        createGroupButton.addClickListener(e -> createGroup(groupName.getValue(), e));
        addComponent(createGroupButton);
        createGroupButton.setEnabled(false);
    }

    private void checkGroupExists(String value) {
        if(value.isEmpty() || Group.checkGroupExists(value)) {
            groupName.setIcon(VaadinIcons.CLOSE_CIRCLE);
            createGroupButton.setEnabled(false);
            grpStatus.setValue("A group with this name already exists.");
        } else {
            groupName.setIcon(VaadinIcons.CHECK_CIRCLE);
            createGroupButton.setEnabled(true);
            grpStatus.setValue("");
        }
    }

    private void createGroup(String groupName, Button.ClickEvent e) {
        Player currentPlayer = null;
        try {
            currentPlayer = DomainRepository.player().findPlayerByID(UUID.fromString(((String) VaadinSession.getCurrent().getAttribute("user-id"))));
        } catch (IOException ex) {
            Notification.show(String.format("Failed to load player: %s - %s:%n%s", ex.getClass().getSimpleName(), ex.getMessage(),
                    ExceptionUtils.getStackTrace(ex)));
            return;
        }

        final Player me = currentPlayer;

        try {
            DomainEventDispatchService.oneshotListenOnCondition(
                    createdEvent ->  (createdEvent instanceof GroupCreated && ((GroupCreated) createdEvent).getCreator().equals(me)),
                    createdEvent -> {
                        UI.getCurrent().getNavigator().navigateTo(EscapismNavigator.DASHBOARD);
                        Notification.show("Group created", Notification.Type.TRAY_NOTIFICATION);
                    }
            );

            Group.build(groupName, currentPlayer);
        } catch (DuplicateGroupException e1) {
            Notification.show("A group of this name already exists.",
                    Notification.Type.ERROR_MESSAGE);
        } catch (DuplicateMemberException e1) {
            Notification.show("This player is already a member of the group.",
                    Notification.Type.ERROR_MESSAGE);
        } catch (InvalidGroupException e2) {
            Notification.show(String.format("An error occurred: %s - %s%n:%s", e.getClass().getSimpleName(), e2.getMessage(),
                    ExceptionUtils.getStackTrace(e2)));
        } catch (GroupNameFormatException e1) {
            Notification.show(String.format("Invalid name for a group: %s", ExceptionUtils.getMessage(e1)),
                    Notification.Type.ERROR_MESSAGE);
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        NewGroupView that = (NewGroupView) o;
        return Objects.equals(groupName, that.groupName) &&
                Objects.equals(createGroupButton, that.createGroupButton) &&
                Objects.equals(grpStatus, that.grpStatus);
    }

    @Override
    public int hashCode() {

        return Objects.hash(super.hashCode(), groupName, createGroupButton, grpStatus);
    }
}
