package io.escapism.domain.player;

import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.junit.Assert.assertThat;

public class HumanNameTest {

    @Test
    public void equalsSameValue() {
        HumanName self = new HumanName("John", "Doe");
        assertThat(self, is(self));

        HumanName notMeButSameValue = new HumanName("John", "Doe");
        assertThat(self, is(notMeButSameValue));
    }

    @Test
    public void notEqualDifferentValue() {
        HumanName john = new HumanName("John", "Doe");
        HumanName jane = new HumanName("Jane", "Doe");
        assertThat(john, not(is(jane)));
    }

    @Test
    public void setGetSurname() {
        HumanName name = new HumanName("John", "Doe");
        HumanName newName = name.withSurname("Jane");
        assertThat(newName.getSurname(), is("Jane"));
        assertThat(name.getSurname(), is("John"));
    }

    @Test
    public void setGetLastname() {
        HumanName name = new HumanName("John", "Doe");
        HumanName newName = name.withLastname("Johnson");
        assertThat(newName.getLastname(), is("Johnson"));
        assertThat(name.getLastname(), is("Doe"));
    }
}
