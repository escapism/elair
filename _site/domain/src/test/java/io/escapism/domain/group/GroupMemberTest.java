package io.escapism.domain.group;

import io.escapism.domain.core.EMailAddress;
import io.escapism.domain.group.*;
import io.escapism.domain.player.HumanName;
import io.escapism.domain.player.Player;
import org.junit.Test;

import java.util.UUID;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.junit.Assert.assertThat;



public class GroupMemberTest {

    @Test
    public void equalsSelf() throws InvalidEMailAddressFormatException, GroupNameFormatException {
        GroupMember member = new GroupMember(new Player(UUID.randomUUID(), new HumanName("John", "Doe")), GroupRole.PLAYER, new Group(UUID.randomUUID(), new GroupName("test")));
        assertThat(member, is(member));
    }

    @Test
    public void notEqualsAnother() throws InvalidEMailAddressFormatException, GroupNameFormatException {
        GroupMember john = new GroupMember(new Player(UUID.randomUUID(), new HumanName("John", "Doe")), GroupRole.PLAYER, new Group(UUID.randomUUID(), new GroupName("test")));
        GroupMember jane = new GroupMember(new Player(UUID.randomUUID(), new HumanName("Jane", "Doe")), GroupRole.PLAYER, new Group(UUID.randomUUID(), new GroupName("test")));
        assertThat(john, not(is(jane)));
    }
}
