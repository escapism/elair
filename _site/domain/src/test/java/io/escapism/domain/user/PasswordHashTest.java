package io.escapism.domain.user;

import org.junit.Test;

import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.junit.Assert.assertThat;

public class PasswordHashTest {

    @Test
    public void hashAndCheck() throws InvalidKeySpecException, NoSuchAlgorithmException {
       Password pass = Password.fromString("test");
       assertThat(pass, is(pass));
       assertThat(pass, is(not(Password.fromString("anotherone"))));
    }
}
