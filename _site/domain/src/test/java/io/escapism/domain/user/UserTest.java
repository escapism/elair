package io.escapism.domain.user;

import io.escapism.domain.core.DomainConfiguration;
import io.escapism.domain.core.DomainEventListener;
import io.escapism.domain.core.EMailAddress;
import io.escapism.domain.core.service.DomainConfigurationService;
import io.escapism.domain.core.service.DomainEventDispatchService;
import io.escapism.domain.group.InvalidEMailAddressFormatException;
import io.escapism.domain.user.event.UserCreated;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.UUID;

import static junit.framework.TestCase.assertTrue;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class UserTest {
    @Mock
    DomainEventListener mockListener;

    @Before
    public void before() {
        DomainConfiguration conf = new DomainConfiguration();
        DomainConfigurationService.configureDomain(conf);
    }

    @Test
    public void equalsSelf() throws InvalidKeySpecException, NoSuchAlgorithmException {
        User user = new User(UUID.randomUUID(), new EMailAddress("john.doe", "example.net"), Password.fromString("test"));
        assertThat(user, is(user));
    }

    @Test
    public void notEqualsOther() throws InvalidKeySpecException, NoSuchAlgorithmException {
        User a = new User(UUID.randomUUID(), new EMailAddress("john.doe", "example.net"), Password.fromString("test"));
        User b = new User(UUID.randomUUID(), new EMailAddress("jane.doe", "example.net"), Password.fromString("test"));
        assertThat(a, not(b));
    }

    @Test
    public void authenticate() throws InvalidKeySpecException, NoSuchAlgorithmException {
        User user = new User(UUID.randomUUID(), new EMailAddress("john.doe", "example.net"), Password.fromString("test"));
        assertTrue(user.authenticate("test"));
        assertFalse(user.authenticate("wrongpw"));
    }


    @Test
    public void build() throws IOException, InvalidEMailAddressFormatException, InvalidKeySpecException, NoSuchAlgorithmException {
        DomainEventDispatchService.listenFor(UserCreated.class, mockListener);

        User user = User.build("john.doe@example.com", "test");

        ArgumentCaptor<UserCreated> usCaptor = ArgumentCaptor.forClass(UserCreated.class);
        verify(mockListener).handleDomainEvent(usCaptor.capture());

        UserCreated capturedEvent = usCaptor.getValue();
        assertThat(capturedEvent.getId(), is(user.getId()));
        assertTrue(user.authenticate("test"));
    }
}
