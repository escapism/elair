package io.escapism.domain.user;

import io.escapism.domain.group.InvalidEMailAddressFormatException;
import io.escapism.domain.user.event.UserCreated;
import org.junit.Test;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.List;
import java.util.UUID;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.instanceOf;
import static org.junit.Assert.assertThat;

public class ActivityStreamTest {

    @Test
    public void equalsSelf() {
        ActivityStream stream = new ActivityStream(UUID.randomUUID());
        assertThat(stream, is(stream));
    }

    @Test
    public void notEqualsOther() {
        ActivityStream self = new ActivityStream(UUID.randomUUID());
        ActivityStream other = new ActivityStream(UUID.randomUUID());
        assertThat(self, is(not(other)));
    }

    @Test
    public void userSignupEvent() throws InvalidKeySpecException, NoSuchAlgorithmException, InvalidEMailAddressFormatException, IOException {
        User user = User.build("john.doe@example.com", "test");
        List<ActivityStreamEntry> entries = user.activityStreamEntries();
        assertThat(entries.size(), greaterThan(0));
        assertThat(entries.get(0).getEvent(), instanceOf(UserCreated.class));
        assertThat(((UserCreated) entries.get(0).getEvent()).getId(), is(user.getId()));
    }
}
