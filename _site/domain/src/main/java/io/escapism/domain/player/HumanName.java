package io.escapism.domain.player;

import java.util.Objects;

public class HumanName {
    private String surname;
    private String lastname;

    public HumanName(String surname, String lastname) {
        this.surname = surname;
        this.lastname = lastname;
    }

    public HumanName withSurname(String surname) {
        return new HumanName(surname, this.lastname);
    }

    public HumanName withLastname(String lastname) {
        return new HumanName(this.surname, lastname);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        HumanName humanName = (HumanName) o;
        return Objects.equals(surname, humanName.surname) &&
                Objects.equals(lastname, humanName.lastname);
    }

    @Override
    public int hashCode() {

        return Objects.hash(surname, lastname);
    }

    public String getSurname() {
        return surname;
    }

    public String getLastname() {
        return lastname;
    }

    @Override
    public String toString() {
        return String.format("%s, %s", lastname, surname);
    }

}
