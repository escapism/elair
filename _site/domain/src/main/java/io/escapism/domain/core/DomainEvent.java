package io.escapism.domain.core;

public abstract class DomainEvent {

    private boolean canceled;

    /**
     * Cancel further dispatching of this event. Any remaining domain event
     * interceptors will not be called and also no listeners will be notified of this event.
     * If the event is already dispatched to the listeners calling this method won't have any
     * effect.
     */
    public void cancel() {
        this.canceled = true;
    }

    public boolean isCanceled() {
        return this.canceled;
    }
}
