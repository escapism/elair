package io.escapism.domain.group.event;

import io.escapism.domain.core.DomainEvent;
import io.escapism.domain.group.Group;
import io.escapism.domain.player.Player;

/**
 * The GroupCreated event informs subscribers that a new group was created
 * within the domain.
 */
public class GroupCreated extends DomainEvent {
    private final Group name;
    private final Player creator;

    public GroupCreated(Group name, Player creator) {
        this.name = name;
        this.creator = creator;
    }

    public Group getGroup() {
        return name;
    }

    public Player getCreator() {
        return creator;
    }
}
