package io.escapism.domain.user;

import io.escapism.domain.core.EMailAddress;

import java.io.IOException;
import java.util.UUID;

public interface UserRepository {
    void saveUser(User user) throws IOException;
    User findUserByID(UUID id) throws IOException;
    User findUserByEmail(EMailAddress mail) throws IOException;
}
