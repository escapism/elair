package io.escapism.domain.group;

public enum GroupRole {
    PLAYER,
    GAMEMASTER
}
