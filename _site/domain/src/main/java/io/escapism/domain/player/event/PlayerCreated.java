package io.escapism.domain.player.event;

import io.escapism.domain.core.DomainEvent;
import io.escapism.domain.player.Player;

public class PlayerCreated extends DomainEvent {
    private final Player player;

    public PlayerCreated(Player pl) {
        this.player = pl;
    }

    public Player getPlayer() {
        return player;
    }
}
