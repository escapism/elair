package io.escapism.domain.user.event;

import io.escapism.domain.core.DomainEvent;

import java.util.UUID;

public class UserCreated extends DomainEvent {

    private final UUID user;

    public UserCreated(UUID user) {
        this.user = user;
    }

    public UUID getId() {
        return user;
    }
}
