package io.escapism.domain.group;

import java.io.IOException;

public interface GroupRepository {
    public void saveGroup(Group group) throws IOException;
    public Group findGroupByName(GroupName name) throws IOException;
}
