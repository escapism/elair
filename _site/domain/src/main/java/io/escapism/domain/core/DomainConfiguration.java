package io.escapism.domain.core;

import io.escapism.domain.group.GroupRepository;
import io.escapism.domain.player.PlayerRepository;
import io.escapism.domain.user.UserRepository;

public class DomainConfiguration {
    private PlayerRepository playerRepository;
    private GroupRepository groupRepository;
    private UserRepository userRepository;

    public void setPlayerRepository(PlayerRepository playerRepository) {
        this.playerRepository = playerRepository;
    }

    public void setGroupRepository(GroupRepository groupRepository) {
        this.groupRepository = groupRepository;
    }

    public GroupRepository getGroupRepository() {
        return groupRepository;
    }

    public PlayerRepository getPlayerRepository() {
        return playerRepository;
    }

    public UserRepository getUserRepository() {
        return userRepository;
    }

    public void setUserRepository(UserRepository userRepository) {
        this.userRepository = userRepository;
    }
}
