package io.escapism.domain.user;

import io.escapism.domain.core.DomainEvent;
import io.escapism.domain.core.service.DomainEventDispatchService;
import io.escapism.domain.user.event.UserCreated;

import java.util.*;

public class ActivityStream {
    private final List<ActivityStreamEntry> entries = new ArrayList<>();
    private final UUID id;

    ActivityStream(UUID ownerId) {
        this.id = ownerId;

        registerEventHandlers();
    }

    private void registerEventHandlers() {
        DomainEventDispatchService.listenOnCondition(
                e -> e instanceof UserCreated && ((UserCreated) e).getId().equals(id),
                e -> addEvent(new Date(), e));
    }

    private synchronized void addEvent(Date date, DomainEvent event) {
        ActivityStreamEntry entry = new ActivityStreamEntry(date, event);
        entries.add(entry);
    }

    public UUID getId() {
        return id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ActivityStream that = (ActivityStream) o;
        return Objects.equals(entries, that.entries) &&
                Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {

        return Objects.hash(entries, id);
    }

    public List<ActivityStreamEntry> getEntries() {
        return entries;
    }
}
