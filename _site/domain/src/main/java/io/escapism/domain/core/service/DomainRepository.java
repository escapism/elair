package io.escapism.domain.core.service;

import io.escapism.domain.group.GroupRepository;
import io.escapism.domain.player.PlayerRepository;
import io.escapism.domain.user.UserRepository;

/**
 * The DomainRepository is a service that provides global access to used repositories.
 *
 * When configuring the domain library you have to specify which repositories will be used
 * in the back via dependency injection.
 *
 * A note on thread-safety: This service does not care. If the underlying repository implementation
 * is not thread-safe the whole domain won't be.
 */
public class DomainRepository {

    private static PlayerRepository player;
    private static GroupRepository group;
    private static UserRepository userRepository;

    private DomainRepository() {} // hidden constructor

    static void setPlayerRepository(PlayerRepository r) {
        player = r;
    }

    static void setGroupRepository(GroupRepository r) {
        group = r;
    }

    public static PlayerRepository player() {
        return player;
    }

    public static GroupRepository group() {
        return group;
    }

    public static void setUserRepository(UserRepository userRepository) {
        DomainRepository.userRepository = userRepository;
    }

    public static UserRepository user() {
        return userRepository;
    }
}
