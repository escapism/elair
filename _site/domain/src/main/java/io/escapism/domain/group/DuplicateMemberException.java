package io.escapism.domain.group;

public class DuplicateMemberException extends InvalidGroupException {
    private final transient GroupMember member;

    public DuplicateMemberException(GroupMember member) {
        super("member of the same GUID already in the group");
        this.member = member;
    }

    public GroupMember getMember() {
        return member;
    }
}
