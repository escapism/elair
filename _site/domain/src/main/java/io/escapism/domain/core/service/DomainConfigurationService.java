package io.escapism.domain.core.service;

import io.escapism.domain.core.DomainConfiguration;

/**
 * This service will bootstrap the domain and configure it for usage.
 *
 * It will actually inject all the necessary dependencies to make the comain
 * work according to the handed ComainConfiguration.
 */
public final class DomainConfigurationService {

    private DomainConfigurationService(){} // hidden constructor

    public static void configureDomain(DomainConfiguration config) {
        configureRepositories(config);
    }

    private static void configureRepositories(DomainConfiguration config) {
        DomainRepository.setGroupRepository(config.getGroupRepository());
        DomainRepository.setPlayerRepository(config.getPlayerRepository());
        DomainRepository.setUserRepository(config.getUserRepository());
    }
}
