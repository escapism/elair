package io.escapism.domain.player;

import io.escapism.domain.core.EMailAddress;

import java.io.IOException;
import java.util.UUID;

public interface PlayerRepository {
    public void savePlayer(Player p) throws IOException;
    public Player findPlayerByID(UUID id) throws IOException;
}
