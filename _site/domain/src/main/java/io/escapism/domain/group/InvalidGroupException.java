package io.escapism.domain.group;

public class InvalidGroupException extends Exception {
    public InvalidGroupException(String message, Throwable cause) {
        super(message, cause);
    }

    public InvalidGroupException(String message) {
        super(message);
    }

    public InvalidGroupException(Throwable cause) {
        super(cause);
    }
}
