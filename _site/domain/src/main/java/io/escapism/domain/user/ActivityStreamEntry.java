package io.escapism.domain.user;

import io.escapism.domain.core.DomainEvent;

import java.util.Date;
import java.util.Objects;

public class ActivityStreamEntry {
    private Date occurance;
    private DomainEvent event;

    public ActivityStreamEntry(Date occurance, DomainEvent event) {
        this.occurance = occurance;
        this.event = event;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ActivityStreamEntry that = (ActivityStreamEntry) o;
        return Objects.equals(occurance, that.occurance) &&
                Objects.equals(event, that.event);
    }

    @Override
    public int hashCode() {
        return Objects.hash(occurance, event);
    }

    @Override
    public String toString() {
        return "ActivityStreamEntry{" +
                "occurance=" + occurance +
                ", event=" + event +
                '}';
    }

    public DomainEvent getEvent() {
        return event;
    }
}
