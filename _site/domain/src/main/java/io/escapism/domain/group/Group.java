package io.escapism.domain.group;

import io.escapism.domain.core.service.DomainEventDispatchService;
import io.escapism.domain.core.service.DomainRepository;
import io.escapism.domain.group.event.GroupCreated;
import io.escapism.domain.player.Player;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.UUID;
import java.util.stream.Collectors;

public class Group {
    private List<GroupMember> members;
    private GroupName name;
    private UUID id;

    public Group(UUID id, GroupName name) {
        this.name = name;
        this.members = new ArrayList<>();
        this.id = id;
    }

    public void addMember(Player pl, GroupRole role) throws DuplicateMemberException {
        int exists = members.stream().filter(m -> m.getPlayer().equals(pl)).collect(Collectors.toList()).size();
        GroupMember member = new GroupMember(pl, role, this);
        if(exists > 0) {
            throw new DuplicateMemberException(member);
        }

        members.add(member);
    }

    public List<GroupMember> getMembers() {
        return members;
    }

    public GroupName getName() {
        return name;
    }

    public void setName(GroupName name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Group group = (Group) o;
        return this.id.equals(group.id);
    }

    @Override
    public int hashCode() {

        return Objects.hash(members, name);
    }

    public static Group build(String name, Player creator) throws InvalidGroupException, GroupNameFormatException {
        GroupName gn = new GroupName(name);

        if(checkGroupExists(name)) {
            throw new DuplicateGroupException(gn);
        }

        Group grp = new Group(UUID.randomUUID(), gn);
        grp.addMember(creator, GroupRole.GAMEMASTER);
        DomainEventDispatchService.dispatchDomainEvent(new GroupCreated(grp, creator));
        return grp;
    }

    public static boolean checkGroupExists(String name) {
        try {
            return DomainRepository.group().findGroupByName(new GroupName(name)) != null;
        } catch (GroupNameFormatException|IOException e) {
            return false; // a group with a wrong name can not exist
        }
    }

    public UUID getId() {
        return id;
    }
}
