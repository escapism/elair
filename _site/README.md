[![pipeline status](https://gitlab.com/escapism/elair/badges/master/pipeline.svg)](https://gitlab.com/escapism/elair/commits/master) ![sonarcloud.io badge](https://sonarcloud.io/api/badges/gate?key=io.escapism:escapism)
# eScapism

eScapism is a communication platform for roleplaying groups that it 
intended to be a place that contains all information for your troupe,
campaign and game!

This repository is used for overall project tracking, planning and
storing resources that are not bound to a specific (technical) part.
