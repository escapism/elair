package io.escapism.ui;

import org.apache.commons.lang3.exception.ExceptionUtils;

import java.util.logging.Logger;

public class DomainRepositoryErrorHandler implements io.escapism.infrastructure.RepositoryErrorHandler {
    private static final Logger logger = Logger.getLogger(DomainRepositoryErrorHandler.class.getName());

    @Override
    public void handleRepositoryException(Exception e) {
        logger.severe(() -> String.format("EXCEPTION: %s - %s", e.getClass().getSimpleName(), e.getMessage()));
        logger.severe(ExceptionUtils.getStackTrace(e));
    }
}
