package io.escapism.ui.view;

import com.vaadin.event.ShortcutAction;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.server.UserError;
import com.vaadin.server.VaadinSession;
import com.vaadin.ui.*;
import com.vaadin.ui.themes.ValoTheme;
import io.escapism.domain.group.InvalidEMailAddressFormatException;
import io.escapism.domain.user.User;
import io.escapism.ui.components.EscapismNavigator;
import org.apache.commons.lang3.exception.ExceptionUtils;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.Objects;

public class RegisterView extends VerticalLayout implements View {

    private final TextField username;
    private final PasswordField password;

    public RegisterView() {
        super();

        setDefaultComponentAlignment(Alignment.MIDDLE_CENTER);

        username = new TextField("E-Mail");
        username.setRequiredIndicatorVisible(true);
        password = new PasswordField("Password");
        password.setRequiredIndicatorVisible(true);

        Button loginButton = new Button("register");
        loginButton.addClickListener(e -> registerUser());
        loginButton.setClickShortcut(ShortcutAction.KeyCode.ENTER);
        loginButton.setStyleName(ValoTheme.BUTTON_PRIMARY);

        Button cancelButton = new Button("cancel");
        cancelButton.addClickListener(clickEvent -> cancelRegistration());
        cancelButton.setStyleName(ValoTheme.BUTTON_DANGER);

        FormLayout layout = new FormLayout();
        layout.addComponent(username);
        layout.addComponent(password);
        layout.setDefaultComponentAlignment(Alignment.MIDDLE_CENTER);

        Panel loginPanel = new Panel("Login");
        loginPanel.setContent(layout);
        loginPanel.setSizeUndefined();

        addComponent(loginPanel);

        HorizontalLayout buttonRow = new HorizontalLayout();
        buttonRow.addComponent(loginButton);
        buttonRow.addComponent(cancelButton);
        addComponent(buttonRow);
    }

    private void cancelRegistration() {
        UI.getCurrent().getNavigator().navigateTo(EscapismNavigator.LOGIN);
    }

    private void registerUser() {
        try {
            User.build(username.getValue(), password.getValue());
        } catch (InvalidEMailAddressFormatException e1) {
            username.setComponentError(new UserError("Invalid E-Mail address"));
        } catch (InvalidKeySpecException | NoSuchAlgorithmException | IOException e1) {
            Notification.show(String.format("Internal Error: %s - %s", e1.getClass().getSimpleName(), e1.getMessage()), ExceptionUtils.getStackTrace(e1), Notification.Type.ERROR_MESSAGE);
            return;
        }

        UI.getCurrent().getNavigator().navigateTo(EscapismNavigator.LOGIN);
        Notification.show("User created", Notification.Type.HUMANIZED_MESSAGE);
    }

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent event) {
        String userId = (String) VaadinSession.getCurrent().getAttribute("user-id");
        if(userId != null) {
            UI.getCurrent().getNavigator().navigateTo(EscapismNavigator.DASHBOARD);
            Notification.show("Please log out first.", "To register a new user you must not be logged in.", Notification.Type.WARNING_MESSAGE);
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        RegisterView that = (RegisterView) o;
        return Objects.equals(username, that.username) &&
                Objects.equals(password, that.password);
    }

    @Override
    public int hashCode() {

        return Objects.hash(super.hashCode(), username, password);
    }
}
