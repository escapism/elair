package io.escapism.ui.view;

import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.server.VaadinSession;
import com.vaadin.ui.Label;
import com.vaadin.ui.VerticalLayout;
import io.escapism.domain.core.service.DomainRepository;
import io.escapism.domain.user.User;
import io.escapism.ui.components.events.EventHistory;
import org.apache.commons.lang3.exception.ExceptionUtils;

import java.io.IOException;
import java.util.UUID;

public class DashboardView extends VerticalLayout implements ProtectedView {
    EventHistory stream;

    public DashboardView() {
        Label title = new Label("Escapism Dashboard");
        title.addStyleName("h1");
        addComponent(title);

        stream = new EventHistory();
        addComponent(stream);
    }

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent event) {
        ProtectedView.super.enter(event);
        String uid = ((String) VaadinSession.getCurrent().getAttribute("user-id"));
        if(uid == null) {
            return;
        }

        User user;
        try {
            user = DomainRepository.user().findUserByID(UUID.fromString(uid));
        } catch (IOException e) {
            Label errorLabel = new Label(String.format("Failed to load user activitiy stream: %s - %s\n%s", e.getClass().getSimpleName(), e.getMessage(), ExceptionUtils.getStackTrace(e)));
            addComponent(errorLabel);
            return;
        }

        stream.loadFor(user);
    }
}
