package io.escapism.ui.components.events;

import io.escapism.domain.core.service.DomainRepository;
import io.escapism.domain.group.Group;
import io.escapism.domain.group.event.GroupCreated;
import io.escapism.domain.player.Player;
import io.escapism.domain.user.ActivityStreamEntry;
import org.apache.commons.lang3.exception.ExceptionUtils;

import java.io.IOException;

public class GroupCreatedEntry extends EventHistoryEntry {
    public GroupCreatedEntry(ActivityStreamEntry event) {
        super(event);

        GroupCreated e = (GroupCreated) event.getEvent();
        Group g;
        try {
            g = DomainRepository.group().findGroupById(e.getGroup());
        } catch (IOException e1) {
            g = null;
            setCaption(String.format("Error: %s - %s", e1.getClass().getSimpleName(), e1.getMessage()));
            setText(ExceptionUtils.getStackTrace(e1));
        }

        String player = "???";
        try {
            Player p = DomainRepository.player().findPlayerByID(e.getCreatorPlayerId());
            player = p.getName().toString();
        } catch (IOException e1) {
            e1.printStackTrace();
        }

        setCaption(String.format("Group created: %s", g.getName().toString()));
        setText(String.format("Group <i>%s</i> was created by <i>%s</i>.", g.getName(), player));
    }
}
