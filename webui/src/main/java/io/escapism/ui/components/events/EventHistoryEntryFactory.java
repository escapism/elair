package io.escapism.ui.components.events;

import io.escapism.domain.group.event.GroupCreated;
import io.escapism.domain.user.ActivityStreamEntry;

public class EventHistoryEntryFactory {
    ActivityStreamEntry e;

    public EventHistoryEntryFactory(ActivityStreamEntry e) {
        this.e = e;
    }

    public EventHistoryEntry build() {
        if(e.getEvent() instanceof GroupCreated) {
            return new GroupCreatedEntry(e);
        } else {
            return new EventHistoryEntry(e);
        }
    }
}
