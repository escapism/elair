package io.escapism.ui.components.events;

import com.vaadin.shared.ui.ContentMode;
import com.vaadin.ui.Label;
import com.vaadin.ui.Panel;
import io.escapism.domain.user.ActivityStreamEntry;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

class EventHistoryEntry extends Panel {

    private final Label content;
    private final String date;

    EventHistoryEntry(ActivityStreamEntry e) {
        DateFormat dfmt = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        date = dfmt.format(e.getOccurance());
        content = new Label(e.getEvent().getClass().getSimpleName());
        content.setContentMode(ContentMode.HTML);
        setContent(content);
    }

    @Override
    public void setCaption(String caption) {
         DateFormat dfmt = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
         caption = String.format("%s - %s", date, caption);
        super.setCaption(caption);
    }

    public void setText(String str) {
        content.setValue(str);
    }
}
