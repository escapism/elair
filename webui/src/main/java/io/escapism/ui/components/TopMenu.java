package io.escapism.ui.components;

import com.vaadin.ui.MenuBar;
import com.vaadin.ui.UI;
import io.escapism.ui.components.EscapismNavigator;

public class TopMenu extends MenuBar {

    public TopMenu(UI ui) {
        addItem("escapism.io", null, menuItem -> ui.getNavigator().navigateTo(EscapismNavigator.DASHBOARD));
        MenuBar.MenuItem groupsItem = addItem("Groups", null, null);
        groupsItem.addItem("My Groups",  menuItem -> ui.getNavigator().navigateTo("MyGroups"));
        groupsItem.addItem("New Group",  menuItem -> ui.getNavigator().navigateTo("NewGroup"));
    }
}
