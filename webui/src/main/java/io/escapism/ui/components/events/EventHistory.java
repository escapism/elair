package io.escapism.ui.components.events;

import com.vaadin.ui.Panel;
import com.vaadin.ui.VerticalLayout;
import io.escapism.domain.user.User;

public class EventHistory extends Panel {

    public EventHistory() {
        setCaption("Activity Stream");
    }

    public void loadFor(User user) {
        VerticalLayout layout = new VerticalLayout();
        setContent(layout);
        if(user != null) {
            user.activityStreamEntries().forEach(e -> {
                EventHistoryEntryFactory factory = new EventHistoryEntryFactory(e);
                layout.addComponent(factory.build());
            });
        }
    }
}