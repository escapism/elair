package io.escapism.domain.core;

public interface DomainEventInterceptor {
    public void interceptDomainEvent(DomainEvent e);
}
