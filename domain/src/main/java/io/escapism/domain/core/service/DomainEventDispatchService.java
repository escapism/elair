package io.escapism.domain.core.service;


import io.escapism.domain.core.DomainEvent;
import io.escapism.domain.core.DomainEventInterceptor;
import io.escapism.domain.core.DomainEventListener;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Predicate;

public class DomainEventDispatchService {
    private static final List<DomainEventInterceptor> interceptors = new ArrayList<>();
    private static final Map<Predicate<DomainEvent>, DomainEventListener> conditionalListeners = new ConcurrentHashMap<>();

    private DomainEventDispatchService() {} // hidden constructor

    public static synchronized void dispatchDomainEvent(DomainEvent event) {
        Class<? extends DomainEvent> cls = event.getClass();

        interceptors.forEach(l -> l.interceptDomainEvent(event));
        if(event.isCanceled()) {
            return;
        }

        conditionalListeners.keySet()
                .stream()
                .filter(p -> p.test(event))
                .map(conditionalListeners::get)
                .forEach(l -> l.handleDomainEvent(event));
    }

    public static synchronized void listenFor(Class<? extends  DomainEvent> type, DomainEventListener listener) {
        listenOnCondition(domainEvent -> domainEvent.getClass() == type, listener);
    }

    public static synchronized void registerInterceptor(DomainEventInterceptor interceptor) {
        interceptors.add(interceptor);
    }

    public static synchronized void listenOnCondition(Predicate<DomainEvent> predicate, DomainEventListener listener) {
        conditionalListeners.put(predicate, listener);
    }

    public static synchronized void oneshotListenOnCondition(Predicate<DomainEvent> condition, DomainEventListener listener) {
        listenOnCondition(condition, e -> {
            listener.handleDomainEvent(e);
            removeListener(listener);
        });
    }

    public static synchronized  void removeListener(DomainEventListener listener) {
        if(listener == null) {
            return;
        }
        conditionalListeners.values().removeIf(v -> listener.equals(v));
    }
}
