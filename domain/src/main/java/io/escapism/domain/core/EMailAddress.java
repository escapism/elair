package io.escapism.domain.core;

import io.escapism.domain.group.InvalidEMailAddressFormatException;
import org.apache.commons.validator.routines.EmailValidator;

import java.util.Objects;

public class EMailAddress {

    private final String user;
    private final String domain;

    public EMailAddress(String user, String domain) {
        this.user = user;
        this.domain = domain;
    }

    public String toString() {
        return String.format("%s@%s", user, domain);
    }

    public static EMailAddress fromString(String str) throws InvalidEMailAddressFormatException {
        if(!EmailValidator.getInstance().isValid(str)) {
            throw new InvalidEMailAddressFormatException(str);
        }

        String[] parts = str.split("@");
        return new EMailAddress(parts[0], parts[1]);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        EMailAddress that = (EMailAddress) o;
        return Objects.equals(user, that.user) &&
                Objects.equals(domain, that.domain);
    }

    @Override
    public int hashCode() {

        return Objects.hash(user, domain);
    }
}
