package io.escapism.domain.core;

import io.escapism.domain.group.GroupRepository;
import io.escapism.domain.player.PlayerRepository;
import io.escapism.domain.user.UserRepository;

public class DomainConfiguration {
    private PlayerRepository playerRepository = new PlayerRepository() {};
    private GroupRepository groupRepository = new GroupRepository() {};
    private UserRepository userRepository = new UserRepository() {};

    public void setPlayerRepository(PlayerRepository playerRepository) {
        this.playerRepository = playerRepository;
    }

    public void setGroupRepository(GroupRepository groupRepository) {
        this.groupRepository = groupRepository;
    }

    public GroupRepository getGroupRepository() {
        return groupRepository;
    }

    public PlayerRepository getPlayerRepository() {
        return playerRepository;
    }

    public UserRepository getUserRepository() {
        return userRepository;
    }

    public void setUserRepository(UserRepository userRepository) {
        this.userRepository = userRepository;
    }
}
