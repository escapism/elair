package io.escapism.domain.core;

public interface DomainEventListener {
    public void handleDomainEvent(DomainEvent event);
}
