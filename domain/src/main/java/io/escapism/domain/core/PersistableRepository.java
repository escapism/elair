package io.escapism.domain.core;

import java.io.Closeable;
import java.io.IOException;

public interface PersistableRepository extends Closeable {
    public void persist() throws IOException;
}
