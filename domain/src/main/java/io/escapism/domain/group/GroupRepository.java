package io.escapism.domain.group;

import java.io.IOException;
import java.util.UUID;

public interface GroupRepository {
    default void saveGroup(Group group) throws IOException {
        return;
    }

    default Group findGroupByName(GroupName name) throws IOException {
        return null;
    }

    default Group findGroupById(UUID group) throws IOException {
        return null;
    }
}
