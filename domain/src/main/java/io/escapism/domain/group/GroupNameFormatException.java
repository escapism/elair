package io.escapism.domain.group;

public class GroupNameFormatException extends Exception {
    public GroupNameFormatException(String name) {
        super(name);
    }
}
