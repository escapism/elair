package io.escapism.domain.group;

public class DuplicateGroupException extends InvalidGroupException {
    private final transient GroupName name;

    public DuplicateGroupException(GroupName gn) {
        super(gn.toString());
        this.name = gn;
    }

    public GroupName getName() {
        return name;
    }
}
