package io.escapism.domain.group;

import io.escapism.domain.player.Player;

import java.util.Objects;

public class GroupMember {
    private Player player;
    private GroupRole role;
    private Group group;

    public GroupMember(Player player, GroupRole role, Group group) {
        this.player = player;
        this.role = role;
        this.group = group;
    }

    public void setRole(GroupRole role) {
        this.role = role;
    }

    public Player getPlayer() {
        return player;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        GroupMember that = (GroupMember) o;
        return Objects.equals(player, that.player) &&
                Objects.equals(group.getName(), that.group.getName());
    }

    @Override
    public int hashCode() {
        return Objects.hash(player.getId(), group.getId());
    }

    public GroupRole getRole() {
        return role;
    }
}
