package io.escapism.domain.group;

import java.util.Objects;

public class GroupName {

    private String name;

    public GroupName(String name) throws GroupNameFormatException {
        name = name.trim();

        if(name.isEmpty()) {
            throw new GroupNameFormatException("must not be empty");
        } else if(name.contains(" ")) {
            throw new GroupNameFormatException("must not contain space");
        }
        this.name = name;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        GroupName groupName = (GroupName) o;
        return Objects.equals(name, groupName.name);
    }

    @Override
    public int hashCode() {

        return Objects.hash(name);
    }

    @Override
    public String toString() {
        return name;
    }


}
