package io.escapism.domain.group.event;

import io.escapism.domain.core.DomainEvent;

import java.util.UUID;


/**
 * The GroupCreated event informs subscribers that a new group was created
 * within the domain.
 */
public class GroupCreated extends DomainEvent {
    private final UUID groupId;
    private final UUID creatorPlayerId;

    public GroupCreated(UUID groupId, UUID creator) {
        this.groupId = groupId;
        this.creatorPlayerId = creator;
    }

    public UUID getGroup() {
        return groupId;
    }

    public UUID getCreatorPlayerId() {
        return creatorPlayerId;
    }
}
