package io.escapism.domain.group;

public class InvalidEMailAddressFormatException extends Exception {
    public InvalidEMailAddressFormatException(String message) {
        super(message);
    }
}
