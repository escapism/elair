package io.escapism.domain.user;

import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import java.math.BigInteger;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.Objects;

public class Password {
    private String hash;

    public Password(String hash) {
        this.hash = hash;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Password password = (Password) o;
        return Objects.equals(hash, password.hash);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(hash);
    }

    public static Password fromString(String password) throws InvalidKeySpecException, NoSuchAlgorithmException {
        String hash = generatePasswordHash(password);
        return new Password(hash);
    }

    private static String generatePasswordHash(String password) throws NoSuchAlgorithmException, InvalidKeySpecException
    {
        int iterations = 1000;
        char[] chars = password.toCharArray();

        // TODO add real salt
        PBEKeySpec spec = new PBEKeySpec(chars, "fixedsalt".getBytes(), iterations, 64 * 8);
        SecretKeyFactory skf = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
        byte[] hash = skf.generateSecret(spec).getEncoded();
        return toHex(hash);
    }

    private static String toHex(byte[] array) {
        BigInteger bi = new BigInteger(1, array);
        String hex = bi.toString(16);
        int paddingLength = (array.length * 2) - hex.length();
        if(paddingLength > 0)
        {
            return String.format(String.format("%%0%dd", paddingLength), 0) + hex;
        }else{
            return hex;
        }
    }

    @Override
    public String toString() {
        return hash;
    }
}
