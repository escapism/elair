package io.escapism.domain.user;

import io.escapism.domain.core.DomainEvent;
import io.escapism.domain.core.EMailAddress;
import io.escapism.domain.core.service.DomainEventDispatchService;
import io.escapism.domain.core.service.DomainRepository;
import io.escapism.domain.group.InvalidEMailAddressFormatException;
import io.escapism.domain.group.event.GroupCreated;
import io.escapism.domain.user.event.UserCreated;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.sql.Date;
import java.util.*;
import java.util.stream.Collectors;

public class User {
    private EMailAddress email;
    private Password password;
    private ActivityStream activityStream;
    private UUID id;

    public User(UUID id, EMailAddress email, Password password) {
        this.email = email;
        this.password = password;
        this.activityStream = new ActivityStream(id);
        this.id = id;
        registerEventHandlers();
    }

    private void registerEventHandlers() {
        DomainEventDispatchService.listenOnCondition(
                e -> e instanceof UserCreated && ((UserCreated) e).getId().equals(id),
                e -> addEventToActivityStream(e));
        DomainEventDispatchService.listenOnCondition(
                e -> e instanceof GroupCreated && ((GroupCreated) e).getCreatorPlayerId().equals(id),
                e -> addEventToActivityStream(e));
    }

    private void addEventToActivityStream(DomainEvent e) {
        activityStream.addEventNow(e);
        try {
            DomainRepository.user().saveUser(this);
        } catch (IOException e1) {
            e1.printStackTrace(); // TODO error handling somehow..
        }
    }


    public EMailAddress getEmail() {
        return email;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return Objects.equals(id, user.id);
    }

    @Override
    public int hashCode() {

        return Objects.hash(email);
    }

    public boolean authenticate(String password) {
        Password hash = null;
        try {
            hash = Password.fromString(password);
        } catch (InvalidKeySpecException | NoSuchAlgorithmException e) {
            return false;
        }
        return hash.equals(this.password);
    }

    public Password getPassword() {
        return password;
    }

    public void setPassword(Password password) {
        this.password = password;
    }

    public static User build(String emailAddress, String password) throws InvalidEMailAddressFormatException, InvalidKeySpecException, NoSuchAlgorithmException, IOException {
        EMailAddress email = EMailAddress.fromString(emailAddress);
        Password pwd = Password.fromString(password);

        User user = new User(UUID.randomUUID(), email, pwd);
        if(DomainRepository.user() != null) {
            DomainRepository.user().saveUser(user);
        }
        DomainEventDispatchService.dispatchDomainEvent(new UserCreated(user.getId()));
        return user;
    }

    public UUID getId() {
        return id;
    }

    @Override
    public String toString() {
        return "User{" +
                "email=" + email +
                ", password=" + password +
                ", activityStream=" + activityStream +
                ", id=" + id +
                '}';
    }

    public List<ActivityStreamEntry> activityStreamEntries() {
        return activityStream.getEntries();
    }

    public List<ActivityStreamEntry> getActivityStreamEventsAfter(Date ts) {
        if(ts == null) {
            return activityStreamEntries();
        }
        return activityStreamEntries().stream().filter(e -> e.getOccurance().compareTo(ts) > 0).collect(Collectors.toList());
    }

    public ActivityStream activityStream() {
        return activityStream;
    }
}
