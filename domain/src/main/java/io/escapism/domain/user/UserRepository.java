package io.escapism.domain.user;

import io.escapism.domain.core.EMailAddress;

import java.io.IOException;
import java.util.UUID;

public interface UserRepository {
    default void saveUser(User user) throws IOException {
        return;
    }

    default User findUserByID(UUID id) throws IOException {
        return null;
    }

    default User findUserByEmail(EMailAddress mail) throws IOException {
        return null;
    }
}
