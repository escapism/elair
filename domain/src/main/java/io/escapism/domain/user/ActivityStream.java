package io.escapism.domain.user;

import io.escapism.domain.core.DomainEvent;

import java.util.*;

public class ActivityStream {
    private final List<ActivityStreamEntry> entries = new ArrayList<>();
    private final UUID id;

    ActivityStream(UUID ownerId) {
        this.id = ownerId;
    }

    public synchronized void addEvent(Date date, DomainEvent event) {
        ActivityStreamEntry entry = new ActivityStreamEntry(date, event);
        entries.add(entry);
    }

    public synchronized  void addEventNow(DomainEvent event) {
        addEvent(Calendar.getInstance().getTime(), event);
    }

    public UUID getId() {
        return id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ActivityStream that = (ActivityStream) o;
        return Objects.equals(entries, that.entries) &&
                Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {

        return Objects.hash(entries, id);
    }

    public List<ActivityStreamEntry> getEntries() {
        return entries;
    }
}
