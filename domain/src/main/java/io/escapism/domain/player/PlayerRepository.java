package io.escapism.domain.player;

import io.escapism.domain.core.EMailAddress;

import java.io.IOException;
import java.util.UUID;

public interface PlayerRepository {
    default void savePlayer(Player p) throws IOException {
        return;
    }

    default Player findPlayerByID(UUID id) throws IOException {
        return null;
    }
}
