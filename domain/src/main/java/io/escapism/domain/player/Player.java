package io.escapism.domain.player;

import io.escapism.domain.core.EMailAddress;
import io.escapism.domain.core.service.DomainEventDispatchService;
import io.escapism.domain.core.service.DomainRepository;
import io.escapism.domain.group.InvalidEMailAddressFormatException;
import io.escapism.domain.player.event.PlayerCreated;

import java.io.IOException;
import java.util.Objects;
import java.util.UUID;

public class Player {
    private HumanName name;
    private UUID id;

    public Player(UUID id, HumanName name) {
        this.name = name;
        this.id = id;
    }

    public void setName(HumanName name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Player player = (Player) o;
        return Objects.equals(id, player.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, id);
    }

    public HumanName getName() {
        return name;
    }

    public static Player build(UUID userId, String forename, String lastname) throws IOException {
        Player pl =  new Player(userId, new HumanName(forename, lastname));

        if(DomainRepository.player() != null) {
            DomainRepository.player().savePlayer(pl);
        }

        PlayerCreated event = new PlayerCreated(pl);
        DomainEventDispatchService.dispatchDomainEvent(event);

        return pl;
    }

    public UUID getId() {
        return id;
    }
}
