package io.escapism.domain.player;

import io.escapism.domain.core.DomainConfiguration;
import io.escapism.domain.core.DomainEventListener;
import io.escapism.domain.core.EMailAddress;
import io.escapism.domain.core.service.DomainConfigurationService;
import io.escapism.domain.core.service.DomainEventDispatchService;
import io.escapism.domain.group.InvalidEMailAddressFormatException;
import io.escapism.domain.player.HumanName;
import io.escapism.domain.player.Player;
import io.escapism.domain.player.event.PlayerCreated;
import io.escapism.domain.user.User;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.UUID;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class PlayerTest {
    @Mock
    DomainEventListener mockListener;

    @Before
    public void before() {
        DomainConfiguration conf = new DomainConfiguration();
        DomainConfigurationService.configureDomain(conf);
    }

    @Test
    public void equalsSelf() {
        Player self = new Player(UUID.randomUUID(), new HumanName("John", "Doe"));
        assertThat(self, is(self));
    }

    @Test
    public void notEqualsAnother() {
        Player john = new Player(UUID.randomUUID(), new HumanName("John", "Doe"));
        Player jane = new Player(UUID.randomUUID(), new HumanName("Jane", "Doe"));
        assertThat(john, not(is(jane)));
    }

    @Test
    public void setGetName() {
        Player player = new Player(UUID.randomUUID(), new HumanName("John", "Doe"));
        player.setName(new HumanName("Jane", "Doe"));
        assertThat(player.getName(), is(new HumanName("Jane", "Doe")));
    }

    @Test
    public void build() throws InvalidEMailAddressFormatException, InvalidKeySpecException, NoSuchAlgorithmException, IOException {
        DomainEventDispatchService.listenFor(PlayerCreated.class, mockListener);
        User user = User.build("john.doe@example.com", "test");
        Player pl = Player.build(user.getId(), "John", "Doe");

        ArgumentCaptor<PlayerCreated> plcCaptor = ArgumentCaptor.forClass(PlayerCreated.class);
        verify(mockListener).handleDomainEvent(plcCaptor.capture());

        PlayerCreated capturedEvent = plcCaptor.getValue();
        assertThat(capturedEvent.getPlayer(), is(pl));
        assertThat(pl.getName(), is(new HumanName("John", "Doe")));
    }

}
