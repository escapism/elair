package io.escapism.domain.group;

import io.escapism.domain.core.DomainConfiguration;
import io.escapism.domain.core.DomainEventListener;
import io.escapism.domain.core.service.DomainConfigurationService;
import io.escapism.domain.core.service.DomainEventDispatchService;
import io.escapism.domain.group.event.GroupCreated;
import io.escapism.domain.player.HumanName;
import io.escapism.domain.player.Player;
import org.hamcrest.core.Is;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.io.IOException;
import java.util.UUID;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class GroupTest {
    @Mock
    GroupRepository mockGroupRepository;
    @Mock
    DomainEventListener mockEventListener;

    @Before
    public void before() {
        DomainConfiguration cf = new DomainConfiguration();
        cf.setGroupRepository(mockGroupRepository);
        DomainConfigurationService.configureDomain(cf);
    }

    @Test
    public void invalidName() {
        try {
            Group grp = new Group(UUID.randomUUID(), new GroupName("contains spaces"));
            assertTrue("spaces in group name are not allowed", false);
        } catch (GroupNameFormatException e) {
            // expected
        }
    }

    @Test
    public void addMember() throws InvalidGroupException, GroupNameFormatException {
        Group group = new Group(UUID.randomUUID(), new GroupName("test"));

        Player pl = new Player(UUID.randomUUID(), new HumanName("test", "test"));
        group.addMember(pl, GroupRole.PLAYER);
        assertThat(group.getMembers().size(), is(1));
        assertThat(group.getMembers().get(0).getPlayer(), is(pl));
        assertThat(group.getMembers().get(0).getRole(), is(GroupRole.PLAYER));
    }

    @Test
    public void addDuplicateMember() throws GroupNameFormatException {
        boolean thrown = false;

        Group group = new Group(UUID.randomUUID(), new GroupName("test"));
        Player pl = new Player(UUID.randomUUID(), new HumanName("test", "test"));
        try {
            group.addMember(pl, GroupRole.PLAYER);
            group.addMember(pl, GroupRole.GAMEMASTER);
        } catch(DuplicateMemberException ex) {
            thrown = true;
        }

        assertThat(thrown, is(true));
    }

    @Test
    public void createGroupWithStringName() throws InvalidGroupException, IOException, GroupNameFormatException {
        doReturn(null).when(mockGroupRepository).findGroupByName(eq(new GroupName("test")));
        DomainEventDispatchService.listenFor(GroupCreated.class, mockEventListener);

        Player creator = new Player(UUID.randomUUID(), new HumanName("John", "Doe"));
        Group grp = Group.build("test", creator);

        ArgumentCaptor<GroupCreated> eventCaptor = ArgumentCaptor.forClass(GroupCreated.class);
        verify(mockEventListener).handleDomainEvent(eventCaptor.capture());

        GroupCreated capturedEvent = eventCaptor.getValue();
        assertNotNull(capturedEvent);
        assertThat(capturedEvent.getGroup(), Is.is(grp.getId()));
        assertThat(capturedEvent.getCreatorPlayerId(), Is.is(creator.getId()));
    }

    @Test
    public void createDuplicateGroup() throws InvalidGroupException, IOException, GroupNameFormatException {
        doReturn(new Group(UUID.randomUUID(), new GroupName("test"))).when(mockGroupRepository).findGroupByName(eq(new GroupName("test")));

        boolean thrown = false;
        try {
            Group.build("test", new Player(UUID.randomUUID(), new HumanName("John", "Doe")));
        } catch(DuplicateGroupException ex) {
            thrown = true;
        }

        assertTrue(thrown);
    }
}
