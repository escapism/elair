package io.escapism.domain.core;

import io.escapism.domain.core.EMailAddress;
import io.escapism.domain.group.InvalidEMailAddressFormatException;
import org.apache.commons.validator.routines.EmailValidator;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;

public class EmailAddressTest {

    @Test
    public void testEquality() {
        EMailAddress e = new EMailAddress("test", "example.com");
        assertThat(e, is(new EMailAddress("test", "example.com")));
    }

    @Test
    public void notEqual() {
        EMailAddress a = new EMailAddress("foo", "bar.com");
        EMailAddress b = new EMailAddress("bar", "foo.at");

        assertThat(a, is(not(b)));
    }

    @Test
    public void testStringRepresetation() {
        EMailAddress e = new EMailAddress("test", "example.com");
        assertThat(e.toString(), is("test@example.com"));
        assertThat(EmailValidator.getInstance().isValid(e.toString()), is(true));
    }

    @Test
    public void fromValidString() throws InvalidEMailAddressFormatException {
        EMailAddress e = EMailAddress.fromString("test@example.com");
        assertThat(e.toString(), is("test@example.com"));
    }

    @Test
    public void fromInvalidString() {
        try {
            EMailAddress.fromString("<invalid address>");
            assertFalse("exception was not thrown", true);
        } catch (InvalidEMailAddressFormatException e) {
            assertThat(e.getMessage(), is("<invalid address>"));
        }
    }
}
