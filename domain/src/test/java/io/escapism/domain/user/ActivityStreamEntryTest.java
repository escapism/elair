package io.escapism.domain.user;

import io.escapism.domain.core.DomainEvent;
import org.junit.Test;

import java.util.Date;
import java.util.UUID;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class ActivityStreamEntryTest {

    @Test
    public void equalsSelf() {
        ActivityStreamEntry entry = new ActivityStreamEntry(new Date(), new DomainEvent() {});
        assertThat(entry, is(entry));
    }

    @Test
    public void equalsSameValue() {
        Date timestamp = new Date();
        DomainEvent event = new DomainEvent() {};
        ActivityStreamEntry a = new ActivityStreamEntry(timestamp, event);
        ActivityStreamEntry b = new ActivityStreamEntry(timestamp, event);
        assertThat(a, is(b));
    }
}
