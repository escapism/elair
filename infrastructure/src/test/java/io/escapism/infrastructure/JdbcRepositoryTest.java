package io.escapism.infrastructure;

import io.escapism.domain.core.DomainConfiguration;
import io.escapism.domain.core.DomainEvent;
import io.escapism.domain.core.EMailAddress;
import io.escapism.domain.core.service.DomainConfigurationService;
import io.escapism.domain.group.*;
import io.escapism.domain.player.HumanName;
import io.escapism.domain.player.Player;
import io.escapism.domain.user.Password;
import io.escapism.domain.user.User;
import io.escapism.domain.user.event.UserCreated;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.sql.SQLException;
import java.util.UUID;
import java.util.concurrent.ThreadLocalRandom;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.instanceOf;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class JdbcRepositoryTest extends InMemoryRepositoryTest {
    @Rule
    public TemporaryFolder folder = new TemporaryFolder();

    private JdbcRepository repo;

    @Before
    public void before() throws SQLException, IOException {
        loadRepository();
    }

    @Test
    public void savePersistLoadAndFindPlayer() throws IOException, SQLException, InvalidEMailAddressFormatException, InvalidKeySpecException, NoSuchAlgorithmException {
        Player pl = createPlayer(repo);
        repo.savePlayer(pl);
        repo.persist();
        repo.close();

        loadRepository();
        Player foundPl = repo.findPlayerByID(pl.getId());

        assertThat(foundPl, is(pl));
    }

    @Test
    public void savePersistLoadAndFindUser() throws SQLException, IOException, InvalidKeySpecException, NoSuchAlgorithmException, InvalidEMailAddressFormatException {
        User user = createDummyUser();
        repo.persist();
        repo.close();
        loadRepository();
        User foundUser = repo.findUserByID(user.getId());

        assertThat(foundUser, is(user));
        assertThat(foundUser.activityStreamEntries().size(), greaterThan(0));
        DomainEvent firstEvent = foundUser.activityStreamEntries().get(0).getEvent();
        assertThat(firstEvent, instanceOf(UserCreated.class));
        assertThat(((UserCreated) firstEvent).getId(), is(user.getId()));
    }

    @Test
    public void findNotExistingUser() throws SQLException, InvalidEMailAddressFormatException, IOException {
        User u = repo.findUserByID(UUID.randomUUID());
        assertThat(u, is(nullValue()));
    }

    private User createDummyUser() throws IOException, SQLException, InvalidKeySpecException, NoSuchAlgorithmException, InvalidEMailAddressFormatException {
        return User.build("john.doe@example.com", "test");
    }

    @Test
    public void persistedUserCanAuthenticate() throws IOException, SQLException, InvalidKeySpecException, NoSuchAlgorithmException, InvalidEMailAddressFormatException {
        User user = createDummyUser();
        assertTrue(user.authenticate("test"));
        assertFalse(user.authenticate("wrongpassword"));
    }


    @Test
    public void savePersistLoadAndFindGroup() throws IOException, SQLException, DuplicateMemberException, InvalidEMailAddressFormatException, GroupNameFormatException, InvalidKeySpecException, NoSuchAlgorithmException {
        Group grp = new Group(UUID.randomUUID(), new GroupName("testGrp"));

        Player plA = createPlayer(repo);
        repo.savePlayer(plA);
        grp.addMember(plA, GroupRole.PLAYER);

        Player plB = createPlayer(repo);
        repo.savePlayer(plB);
        grp.addMember(plB, GroupRole.GAMEMASTER);

        repo.saveGroup(grp);
        repo.persist();
        repo.close();

        loadRepository();
        Group foundGrp = repo.findGroupByName(grp.getName());

        assertThat(foundGrp, is(grp));
    }

    private Player createPlayer(JdbcRepository repo) throws InvalidEMailAddressFormatException, IOException, InvalidKeySpecException, NoSuchAlgorithmException {
        RandomString gen = new RandomString(25, ThreadLocalRandom.current());
        EMailAddress address = EMailAddress.fromString(String.format("%s@%s.com", gen.nextString(), gen.nextString()));
        User u = new User(UUID.randomUUID(), address, Password.fromString("test"));
        try {
            u.setPassword(Password.fromString("test"));
        } catch (InvalidKeySpecException | NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        repo.saveUser(u);
        return new Player(u.getId(), new HumanName(gen.nextString(), gen.nextString()));
    }

    @Override
    protected void loadRepository() {
        if(repo != null) {
            try {
                repo.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        try {
            repo = new JdbcRepository(String.format("jdbc:h2:%s", folder.getRoot().getAbsolutePath()));
        } catch (SQLException e) {
            e.printStackTrace();
        }
        DomainConfiguration conf = new DomainConfiguration();
        conf.setUserRepository(repo);
        conf.setPlayerRepository(repo);
        DomainConfigurationService.configureDomain(conf);

        inMemoryRepository = repo;
    }
}
