package io.escapism.infrastructure;

import io.escapism.domain.core.DomainConfiguration;
import io.escapism.domain.core.EMailAddress;
import io.escapism.domain.core.service.DomainConfigurationService;
import io.escapism.domain.core.service.DomainEventDispatchService;
import io.escapism.domain.group.*;
import io.escapism.domain.group.event.GroupCreated;
import io.escapism.domain.player.HumanName;
import io.escapism.domain.player.Player;
import io.escapism.domain.player.event.PlayerCreated;
import io.escapism.domain.user.User;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.sql.SQLException;
import java.util.UUID;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.notNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class InMemoryRepositoryTest {
    protected InMemoryRepository inMemoryRepository;

    @Before
    public void before() throws SQLException, IOException {
        loadRepository();
    }

    protected void loadRepository() {
        inMemoryRepository = new InMemoryRepository();

        DomainConfiguration conf = new DomainConfiguration();
        conf.setUserRepository(inMemoryRepository);
        conf.setPlayerRepository(inMemoryRepository);

        DomainConfigurationService.configureDomain(conf);
    }

    @Test
    public void saveAndFindPlayer() throws IOException, InvalidKeySpecException, NoSuchAlgorithmException, InvalidEMailAddressFormatException {
        User user = User.build("john.doe@example.com", "test");
        Player pl = new Player(user.getId(), new HumanName("John", "Doe"));
        inMemoryRepository.savePlayer(pl);
        Player foundPl = inMemoryRepository.findPlayerByID(pl.getId());

        assertThat(foundPl, is(pl));
    }

    @Test
    public void saveAndFindGroup() throws DuplicateMemberException, IOException, GroupNameFormatException, InvalidKeySpecException, NoSuchAlgorithmException, InvalidEMailAddressFormatException {
        User user = User.build("johm.dow@example.com", "test");
        Player pl = Player.build(user.getId(), "John", "Dow");

        Group grp = new Group(UUID.randomUUID(), new GroupName("foo"));
        grp.addMember(pl, GroupRole.PLAYER);
        inMemoryRepository.saveGroup(grp);

        Group foundGrp = inMemoryRepository.findGroupByName(grp.getName());

        assertThat(foundGrp, is(grp));
    }

    @Test
    public void findGroupById() throws InvalidEMailAddressFormatException, InvalidKeySpecException, NoSuchAlgorithmException, IOException, GroupNameFormatException, DuplicateMemberException {
        User user = User.build("johm.dow@example.com", "test");
        Player pl = Player.build(user.getId(), "John", "Dow");

        Group grp = new Group(UUID.randomUUID(), new GroupName("foo"));
        grp.addMember(pl, GroupRole.PLAYER);
        inMemoryRepository.saveGroup(grp);

        Group foundGrp = inMemoryRepository.findGroupById(grp.getId());

        assertThat(foundGrp, is(grp));
    }

    @Test
    public void findUserByMail() throws InvalidKeySpecException, NoSuchAlgorithmException, InvalidEMailAddressFormatException, IOException {
        User u = User.build("john.doe@example.com", "test");
        assertThat(inMemoryRepository.findUserByEmail(new EMailAddress("john.doe", "example.com")), is(u));
    }
}
