package io.escapism.infrastructure;

public interface RepositoryErrorHandler {
    public void handleRepositoryException(Exception e);
}
