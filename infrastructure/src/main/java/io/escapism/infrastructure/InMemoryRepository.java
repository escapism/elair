package io.escapism.infrastructure;

import io.escapism.domain.core.DomainEvent;
import io.escapism.domain.core.DomainEventInterceptor;
import io.escapism.domain.core.EMailAddress;
import io.escapism.domain.core.service.DomainEventDispatchService;
import io.escapism.domain.group.Group;
import io.escapism.domain.group.GroupName;
import io.escapism.domain.group.GroupRepository;
import io.escapism.domain.group.event.GroupCreated;
import io.escapism.domain.player.Player;
import io.escapism.domain.player.PlayerRepository;
import io.escapism.domain.player.event.PlayerCreated;
import io.escapism.domain.user.User;
import io.escapism.domain.user.UserRepository;
import io.escapism.domain.user.event.UserCreated;

import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

public class InMemoryRepository implements PlayerRepository, GroupRepository, UserRepository {
    final Map<UUID, Player> playerRepo = new HashMap<>();
    final Map<UUID, Group> groupRepo = new HashMap<>();
    final Map<UUID, User> userRepo = new HashMap<>();
    private final List<RepositoryErrorHandler> errorHandlerList = new ArrayList<>();

    InMemoryRepository() {
    }

    private void dispatchException(Exception e) {
        errorHandlerList.parallelStream().forEach(h -> h.handleRepositoryException(e));
    }

    @Override
    public void savePlayer(Player p) throws IOException {
        playerRepo.put(p.getId(), p);
    }

    @Override
    public Player findPlayerByID(UUID id) throws IOException {
        return playerRepo.get(id);
    }

    @Override
    public void saveGroup(Group group) throws IOException {
        groupRepo.put(group.getId(), group);
    }

    @Override
    public Group findGroupByName(GroupName name) throws IOException {
        List<Group> results = groupRepo.values().stream().filter(g -> g.getName().equals(name)).collect(Collectors.toList());
        if(results.size() < 1) {
            return null;
        }
        return results.get(0);
    }

    @Override
    public Group findGroupById(UUID id) throws IOException {
        List<Group> results = groupRepo.values().stream().filter(g -> g.getId().equals(id)).collect(Collectors.toList());
        if(results.size() < 1) {
            return null;
        }
        return results.get(0);
    }

    @Override
    public void saveUser(User user) throws IOException {
        userRepo.put(user.getId(), user);
    }

    @Override
    public User findUserByID(UUID id) throws IOException {
        return userRepo.get(id);
    }

    @Override
    public User findUserByEmail(EMailAddress mail) throws IOException {
        List<User> results = userRepo.values().stream().filter(u -> u.getEmail().equals(mail)).collect(Collectors.toList());
        if(results.size() < 1) {
            return null;
        }
        return results.get(0);
    }

    public void addErrorHandler(RepositoryErrorHandler handler) {
        if (!errorHandlerList.contains(handler)) {
            errorHandlerList.add(handler);
        }
    }

}
