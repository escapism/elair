package io.escapism.infrastructure;

import com.google.gson.Gson;
import io.escapism.domain.core.DomainEvent;
import io.escapism.domain.core.EMailAddress;
import io.escapism.domain.core.PersistableRepository;
import io.escapism.domain.group.*;
import io.escapism.domain.player.HumanName;
import io.escapism.domain.player.Player;
import io.escapism.domain.user.ActivityStreamEntry;
import io.escapism.domain.user.Password;
import io.escapism.domain.user.User;
import org.flywaydb.core.Flyway;
import org.flywaydb.core.api.MigrationInfoService;
import org.h2.command.Prepared;

import java.io.IOException;
import java.sql.*;
import java.util.List;
import java.util.Map;
import java.util.UUID;

// TODO maybe use stragies instead of complex extension and implementation...
public class JdbcRepository extends InMemoryRepository implements PersistableRepository {

    private final Connection conn;

    public JdbcRepository(String jdbcUrl) throws SQLException {
        try {
            Class.forName("org.h2.Driver");
        } catch (ClassNotFoundException e) {
            throw new RuntimeException("failed to load h2 jdbc driver");
        }

        conn = DriverManager.getConnection(jdbcUrl, "sa", "");
        migrateDatabase(jdbcUrl);

        readDatabase();
    }

    private void readDatabase() throws SQLException {
        readPlayerRepository();
        readGroupRepository();
        readUserRepository();
    }

    private void readUserRepository() throws SQLException {
        PreparedStatement stmnt = conn.prepareStatement("SELECT * FROM users");
        ResultSet rs = stmnt.executeQuery();
        while (rs.next()) {
            User user = null;
            try {
                user = buildUser(rs);
            } catch (IOException e) {
                throw new SQLException("failed reading user record", e);
            }
            userRepo.put(user.getId(), user);
        }
    }

    private void readGroupRepository() throws SQLException {
        PreparedStatement stmnt = conn.prepareStatement("SELECT * FROM groups");
        ResultSet rs = stmnt.executeQuery();
        while (rs.next()) {
            Group grp = null;
            try {
                grp = buildGroup(rs);
            } catch (DuplicateMemberException e) {
                throw new SQLException("constraint violated: duplicate member in group", e);
            } catch (InvalidEMailAddressFormatException e) {
                throw new SQLException(String.format("invalid format for mail address: %s", e.getMessage()), e);
            } catch (GroupNameFormatException e) {
                throw new SQLException(String.format("invalid format for group name (%s): %s", rs.getString("name"), e.getMessage()));
            } catch (IOException e) {
                throw new SQLException("failed to access data", e);
            }
            groupRepo.put(grp.getId(), grp);
        }
    }

    private Group buildGroup(ResultSet rs) throws SQLException, DuplicateMemberException, InvalidEMailAddressFormatException, GroupNameFormatException, IOException {
        Group grp = new Group(UUID.fromString(rs.getString("id")), new GroupName(rs.getString("name")));
        addMembersToGroup(grp);
        return grp;
    }

    private void addMembersToGroup(Group grp) throws SQLException, DuplicateMemberException, IOException {
        PreparedStatement stmnt = conn.prepareStatement("SELECT * FROM group_member WHERE groupId = ?");
        stmnt.setString(1, grp.getId().toString());
        ResultSet rs = stmnt.executeQuery();
        while (rs.next()) {
            Player pl = findPlayerByID(UUID.fromString(rs.getString("playerId")));
            GroupRole role = GroupRole.valueOf(rs.getString("role"));
            grp.addMember(pl, role);
        }
    }

    private void readPlayerRepository() throws SQLException {
        PreparedStatement stmnt = conn.prepareStatement("SELECT * FROM player");
        ResultSet rs = stmnt.executeQuery();
        while (rs.next()) {
            Player pl = buildPlayer(rs);
            playerRepo.put(pl.getId(), pl);
        }
    }

    private void migrateDatabase(String jdbcUrl) throws SQLException {
        Flyway fw = new Flyway();
        fw.setDataSource(jdbcUrl, "sa", "");
        MigrationInfoService info = fw.info();

        int pending = info.pending().length;
        if (pending > 0) {
            int success = fw.migrate();
            if (success != pending) {
                throw new SQLException("database migration failed");
            }
        }
    }

    public void savePlayerRecord(Player p) throws IOException {
        try {
            PreparedStatement stmnt;
            if (queryPlayer(p.getId()) != null) {
                stmnt = conn.prepareStatement("UPDATE player set surname = ?, lastname = ? where id = ?");
                stmnt.setString(1, p.getName().getSurname());
                stmnt.setString(2, p.getName().getLastname());
                stmnt.setString(3, p.getId().toString());
            } else {
                stmnt = conn.prepareStatement("INSERT INTO player(id, surname, lastname) values(?, ?, ?)");
                stmnt.setString(1, p.getId().toString());
                stmnt.setString(2, p.getName().getSurname());
                stmnt.setString(3, p.getName().getLastname());
            }

            if (stmnt.executeUpdate() < 1) {
                throw new IOException("failed to save player");
            }
        } catch (SQLException e) {
            throw new IOException("saving player failed", e);
        }
    }

    private Player queryPlayer(UUID id) throws IOException {
        try {
            PreparedStatement stmnt = conn.prepareStatement("SELECT * from player where id = ?");
            stmnt.setString(1, id.toString());
            ResultSet rs = stmnt.executeQuery();
            if (rs.next()) {
                return buildPlayer(rs);
            } else {
                return null;
            }
        } catch (SQLException e) {
            throw new IOException("failed to query player", e);
        }
    }

    private Player buildPlayer(ResultSet rs) throws SQLException {
        return new Player(UUID.fromString(rs.getString("id")), new HumanName(rs.getString("surname"), rs.getString("lastname")));
    }

    @Override
    public void persist() throws IOException {
        for (Map.Entry<UUID, Player> entry : playerRepo.entrySet()) {
            savePlayerRecord(entry.getValue());
        }

        for (Map.Entry<UUID, Group> entry : groupRepo.entrySet()) {
            saveGroupRecord(entry.getValue());
        }

        for (Map.Entry<UUID, User> entry : userRepo.entrySet()) {
            saveUserRecord(entry.getValue());
        }
    }

    private void saveUserRecord(User value) throws IOException {
        updateActivityStream(value);
        PreparedStatement stmnt;
        if (queryUser(value.getId()) != null) {
            return;
        }

        try {
            stmnt = conn.prepareStatement("INSERT INTO users(id, email, password) VALUES(?, ?, ?)");
            stmnt.setString(1, value.getId().toString());
            stmnt.setString(2, value.getEmail().toString());
            stmnt.setString(3, value.getPassword().toString());
            stmnt.executeUpdate();
        } catch (SQLException e) {
            throw new IOException("failed to save user record", e);
        }
    }

    private void updateActivityStream(User user) throws IOException {
        Date lastTimestamp = getLatestTimestampFromActivitiyStream(user.getId());
        List<ActivityStreamEntry> newEvents = user.getActivityStreamEventsAfter(lastTimestamp);

        try {
            for (ActivityStreamEntry entry : newEvents) {
                PreparedStatement stmnt = conn.prepareStatement("INSERT INTO activity_stream(id, occurance, type, event) values(?, ?, ?, ?)");
                stmnt.setString(1, user.getId().toString());
                stmnt.setDate(2, new Date(entry.getOccurance().getTime()));
                stmnt.setString(3, entry.getEvent().getClass().getCanonicalName());

                Gson gson = new Gson();
                String evntJson = gson.toJson(entry.getEvent());
                stmnt.setString(4, evntJson);

                stmnt.executeUpdate();
            }
        } catch (SQLException e) {
            throw new IOException("failed to save user stream", e);
        }
    }

    private Date getLatestTimestampFromActivitiyStream(UUID id) throws IOException {
        try {
            PreparedStatement stmnt = conn.prepareStatement("SELECT MAX(occurance) FROM activity_stream where id = ?");
            stmnt.setString(1, id.toString());
            ResultSet rs = stmnt.executeQuery();
            if (!rs.next()) {
                return new Date(0);
            }

            return rs.getDate(1);
        } catch (SQLException e) {
            throw new IOException("failed to read latest stream entry", e);
        }
    }

    private User queryUser(UUID id) throws IOException {

        PreparedStatement stmnt = null;
        ResultSet res;

        try {
            stmnt = conn.prepareStatement("SELECT * FROM users where id = ?");
            stmnt.setString(1, id.toString());
            res = stmnt.executeQuery();
            if (!res.next()) {
                return null;
            }
        } catch (SQLException e) {
            throw new IOException("failed to query user record", e);
        }

        return buildUser(res);
    }

    private User buildUser(ResultSet res) throws IOException {
        User user = null;
        try {
            user = new User(UUID.fromString(res.getString("id")), EMailAddress.fromString(res.getString("email")), new Password(res.getString("password")));
            populateActivityStream(user);
        } catch (InvalidEMailAddressFormatException e) {
            throw new IOException("illegal mail address format for user", e);
        } catch (SQLException e) {
            throw new IOException("failed to fetch user data", e);
        } catch (ClassNotFoundException e) {
            throw new IOException("invalid event type in user activity stream", e);
        }
        return user;
    }

    private void populateActivityStream(User user) throws SQLException, ClassNotFoundException {
        PreparedStatement stmnt = conn.prepareStatement("SELECT * FROM activity_stream where id = ?");
        stmnt.setString(1, user.getId().toString());
        ResultSet rs = stmnt.executeQuery();
        while(rs.next()) {
            String className = rs.getString("type");
            Class<?> cls = Class.forName(className);
            Gson gson = new Gson();
            DomainEvent event = (DomainEvent) gson.fromJson(rs.getString("event"), cls);
            user.activityStream().addEvent(new Date(rs.getDate("occurance").getTime()), event);
        }
    }

    private void saveGroupRecord(Group group) throws IOException {
        try {
            if (queryGroup(group.getName()) == null) {
                PreparedStatement stmnt = conn.prepareStatement("INSERT INTO groups(id, name) values(?, ?)");
                stmnt.setString(1, group.getId().toString());
                stmnt.setString(2, group.getName().toString());
                stmnt.executeUpdate();

                saveGroupMembers(group);
            }
        } catch (SQLException e) {
            throw new IOException("failed to save group", e);
        }
    }

    private void saveGroupMembers(Group group) throws SQLException {
        for (GroupMember member : group.getMembers()) {
            PreparedStatement delete = conn.prepareStatement("DELETE FROM group_member WHERE groupId = ? AND playerId = ?");
            delete.setString(1, group.getId().toString());
            delete.setString(2, member.getPlayer().getId().toString());
            delete.executeUpdate();

            PreparedStatement insert = conn.prepareStatement("INSERT INTO group_member(playerId, groupId, role) values(?,?,?)");
            insert.setString(1, member.getPlayer().getId().toString());
            insert.setString(2, group.getId().toString());
            insert.setString(3, member.getRole().name());
            insert.executeUpdate();
        }
    }

    private Group queryGroup(GroupName name) throws IOException {
        PreparedStatement stmnt = null;
        ResultSet rs = null;
        try {
            stmnt = conn.prepareStatement("SELECT * FROM groups WHERE name = ?");
            stmnt.setString(1, name.toString());
            rs = stmnt.executeQuery();
            if (!rs.next()) {
                return null;
            }
        } catch (SQLException e) {
            throw new IOException("failed to query group data", e);
        }

        Group g = null;
        try {
            g = buildGroup(rs);
        } catch (SQLException | DuplicateMemberException | InvalidEMailAddressFormatException | GroupNameFormatException e) {
            throw new IOException("group consistency requirements failed", e);
        }
        return g;
    }

    @Override
    public void close() throws IOException {
        try {
            conn.close();
        } catch (SQLException ex) {
            throw new IOException("could not close database", ex);
        }
    }

    @Override
    public void savePlayer(Player p) throws IOException {
        super.savePlayer(p);
        savePlayerRecord(p);
    }

    @Override
    public void saveGroup(Group group) throws IOException {
        super.saveGroup(group);
        saveGroupRecord(group);
    }

    @Override
    public void saveUser(User user) throws IOException {
        super.saveUser(user);
        saveUserRecord(user);
    }

    @Override
    public User findUserByID(UUID id) throws IOException {
        User u = super.findUserByID(id);
        if (u == null) {
            u = queryUser(id);
        }

        return u;
    }

    @Override
    public Player findPlayerByID(UUID id) throws IOException {
        Player pl = super.findPlayerByID(id);
        if (pl == null) {
            pl = queryPlayer(id);
        }

        return pl;
    }

    @Override
    public Group findGroupByName(GroupName name) throws IOException {
        Group grp = super.findGroupByName(name);
        if (grp == null) {
            grp = queryGroup(name);
        }
        return grp;
    }

    @Override
    public Group findGroupById(UUID id) throws IOException {
        Group grp = super.findGroupById(id);
        if(grp == null) {
            grp = queryGroupById(id);
        }
        return grp;
    }

    private Group queryGroupById(UUID id) throws IOException {
        PreparedStatement stmnt = null;
        ResultSet rs = null;
        try {
            stmnt = conn.prepareStatement("SELECT * FROM groups WHERE id = ?");
            stmnt.setString(1, id.toString());
            rs = stmnt.executeQuery();
            if (!rs.next()) {
                return null;
            }
        } catch (SQLException e) {
            throw new IOException("failed to query group data", e);
        }

        Group g = null;
        try {
            g = buildGroup(rs);
        } catch (SQLException | DuplicateMemberException | InvalidEMailAddressFormatException | GroupNameFormatException e) {
            throw new IOException("group consistency requirements failed", e);
        }
        return g;
    }
}
