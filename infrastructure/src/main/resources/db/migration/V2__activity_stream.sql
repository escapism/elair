create table activity_stream (
     id varchar(512) not null,
     occurance timestamp not null,
     type varchar(512) not null,
     event clob
)