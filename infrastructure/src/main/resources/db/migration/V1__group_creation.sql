create table users (
    id varchar(512),
    email varchar(256),
    password varchar(4069),
    primary key(id)
);

create table player (
    id varchar(512),
    surname varchar(4096),
    lastname varchar(4096),

    primary key(id),
    foreign key(id) references users(id)
);

create table groups (
    id varchar(512),
    name varchar(4096),
    primary key(name)
);

create table group_member (
    playerId  varchar(512),
    groupId   varchar(512),
    role      varchar(256),

    primary key(playerId, groupId),
    foreign key(playerId) references player(id),
    foreign key(groupId) references groups(id)
);

-- master player
insert into users(id, email, password) values('66a8aaaf-e664-479b-95b4-041373be23b0', 'master@example.com', '5665547a5a19e06e73fccfc0b4e695276851af0491b164d4812d5c0da879fb6c7b127d997544b55966e7b763e7cd074ffda005c5dacfa7a2e17daae3c66d7595');
insert into player(id, surname, lastname) values('66a8aaaf-e664-479b-95b4-041373be23b0', 'Master', 'Escapism');